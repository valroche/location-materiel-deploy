\! echo "[SQL] Creating db";
create database if not exists location_materiel_db;
\! echo "[SQL] Creating user and granting privileges"
CREATE USER IF NOT EXISTS 'locationapp'@'%' IDENTIFIED BY 'locationapp';
GRANT ALL PRIVILEGES ON location_materiel_db.* TO 'locationapp'@'%' IDENTIFIED BY 'locationapp';
FLUSH PRIVILEGES;
