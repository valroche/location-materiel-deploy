#!/bin/bash
#apt-get upgrade
#apt-get update

# Installation des dépendances liées à Maven/Spring/Java tmtc
apt-get install -y default-jdk maven

java -version
mvn -version

apt-get install -y mariadb-server
mysql --version

mysql_secure_install

git clone https://gitlab.com/CorentinFs/location-material-backend
cd location-material-backend/
git checkout deploy #a retirer quand les changements auront été merge

# Création de l'utilisateur et de la bdd
cd ..
mysql -u root -ppassword < "init_db.sql"

# Lancement du serveur maven en arrière plan avec les paramètres de production
cd location-material-backend/
cmd_launch_mvn = "mvn spring-boot:run -Pprod -Dspring-boot.run.profiles=prod -Djdbc.url='jdbc:mysql://localhost:3306/location_materiel_db?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC'"

cmd_launch_mvn &

# Récupération du projet pour le front
cd ..
git clone https://gitlab.com/valroche/location-materiel.git
git checkout dev

apt-get install curl

curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs
apt-get install npm
cd location-materiel/location-materiel

npm install @vue/cli
npm install
npm run build

cp -r dist /var/www/

cp ../../loc /etc/nginx/sites-enabled
rm /etc/nginx/sites-enabled/default

systemctl restart nginx



